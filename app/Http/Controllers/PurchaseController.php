<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Purchase;
use App\Models\Wac;

class PurchaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|date',
            'product_name' => 'required|string',
            'quantity' => 'required|integer|min:1',
            'unit_cost' => 'required|numeric|min:0'
        ]);

        $purchase = new Purchase;
        $purchase->product_name = $request->product_name;
        $purchase->date = $request->date;
        $purchase->quantity = $request->quantity;
        $purchase->unit_cost = $request->unit_cost;
        $purchase->total_cost = $request->unit_cost * $request->quantity;
        $purchase->save();

        $wac = Wac::where('product_name', $request->product_name)->count();

        if ($wac === 0) {
            $wac = new Wac;
            $wac->product_name = $request->product_name;
            $wac->date = $request->date;
            $wac->total_value = $request->unit_cost * $request->quantity;
            $wac->total_quantity = $request->quantity;
            $wac->average_cost = $request->unit_cost;
            $wac->save();
        } else {
            $wac_update = Wac::where('product_name', $request->product_name)->first();

            $new_total_value = $wac_update->total_value + ($request->unit_cost * $request->quantity);
            $new_total_quantity = $wac_update->total_quantity + $request->quantity;

            $wac_update->date = $request->date;
            $wac_update->total_value = $new_total_value;
            $wac_update->total_quantity = $wac_update->total_quantity + $request->quantity;
            $wac_update->average_cost = $new_total_value / $new_total_quantity;
            $wac_update->save();
        }

        return response()->json(['message' => 'Purchase transaction recorded']);
    }

    public function index()
    {
        $purchases = Purchase::all();
        return response()->json($purchases);
    }
}
