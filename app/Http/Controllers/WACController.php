<?php

namespace App\Http\Controllers;

use App\Models\Wac;

class WACController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function get_all_wacs()
    {
        $wacs = Wac::all();
        return response()->json($wacs);
    }
}
