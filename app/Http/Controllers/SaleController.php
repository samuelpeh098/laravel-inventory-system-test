<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sale;
use App\Models\Wac;


class SaleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index()
    {
        $sales = Sale::all();
        return response()->json($sales);
    }

    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required|date',
            'product_name' => 'required|string',
            'quantity' => 'required|integer|min:1',
        ]);

        $wac = Wac::where('product_name', $request->product_name)->first();

        // To check available stock
        if ($request->quantity > $wac->total_quantity) {
            return response()->json(['message' => 'Quantity Requested Exceeded Current Stock.']);
        }
        $total_sales = $wac->average_cost * $request->quantity;

        $sale = new Sale;
        $sale->product_name = $request->product_name;
        $sale->date = $request->date;
        $sale->quantity = $request->quantity;
        $sale->average_price = $wac->average_cost;
        $sale->total_sales = $total_sales;
        $sale->save();

        $wac->total_value = $wac->total_value - $total_sales;
        $wac->total_quantity = $wac->total_quantity - $request->quantity;
        $wac->average_cost = ($wac->total_value - $total_sales) / ($wac->total_quantity - $request->quantity);
        $wac->save();

        return response()->json(['message' => 'Sale transaction recorded']);
    }
}
