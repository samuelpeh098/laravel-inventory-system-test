<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wac extends Model
{
    use HasFactory;
    protected $table = 'wacs';
    protected $fillable = ['total_value', 'total_quantity', 'average_cost', 'product_name'];
}
