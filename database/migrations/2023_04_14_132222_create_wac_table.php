<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('wacs', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            $table->decimal('total_value', 10, 2);
            $table->integer('total_quantity');
            $table->decimal('average_cost', 10, 2);
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('wacs');
    }
};
