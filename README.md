# Laravel Inventory System Test

## Getting started
Please ensure Laravel 10 is installed in your PC.
Next is to ensure MYSQL is properly setup and prepare the credentials for MYSQL Connection in This Web App.

## Name
Laravel Inventory System Test

## Description
This is a system backend system, where it enables users to:
- Regiter
- Login
- Add New Purchase
- Get Purchase List
- Add New Sales
- Get All Sales
- Get All WAC based on product

## Installation
Write those credentials of MYSQL Connections in .env file
Then run the following command:
```
composer install
npm install
php artisan migrate
php artisan serve
```

## Postman Collection will be provided
A JSON file postman with all the proper APIs will be provided