<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\WACController;

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');
});

Route::controller(PurchaseController::class)->group(function () {
    Route::get('get_purchases', 'index');
    Route::post('save_purchase', 'store');
});

Route::controller(SaleController::class)->group(function () {
    Route::get('get_sales', 'index');
    Route::post('save_sale', 'store');
});

Route::controller(WACController::class)->group(function () {
    Route::get('get_all_wacs', 'get_all_wacs');
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
